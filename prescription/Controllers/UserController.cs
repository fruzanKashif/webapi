﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using prescription.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prescription.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
       
        private readonly ILogger<WeatherForecastController> _logger;
        public UserController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<User> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new User
            {

            }).ToArray();
        }

        [HttpPost("save")]
        public IActionResult SaveUsers(User user)
        {
            return Json(new { retCode = 1,  });
        }
    }
}
