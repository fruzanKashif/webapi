﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace prescription.Model
{
    public class User
    {
        public string id { get; set; }
        public string name { get; set; }
        public string userType { get; set; }

    }
}
